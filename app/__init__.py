import os
import requests
import feedparser
import urllib
import json
import lxml.html

from pprint import pprint
from flask import Flask, Blueprint, Response, render_template, request, jsonify
from geojson import Feature, FeatureCollection, Point
from collections import defaultdict
from app.api.restplus import api

app = Flask(__name__)

blueprint = Blueprint('api', __name__, url_prefix='/api')
api.init_app(blueprint)
app.register_blueprint(blueprint)

categories = []

with open(os.path.dirname(os.path.realpath(__file__)) + "/categories.txt", "r") as f:
    for line in f.readlines():
        cat = tuple(line.split(":"))
        categories.append(cat)

authors = {}

with open(os.path.dirname(os.path.realpath(__file__)) + "/authors.txt", "r") as f:
    for line in f.readlines():
        author, uni = tuple(line.split(","))
        authors[author] = uni

print(authors)

@app.route('/', methods=['GET', 'POST'])
def index():
    return render_template('index.html', categories=categories)


@app.route('/search', methods = ['POST'])
def search():
    field_value = request.form['category']
    cat = field_value.split(":")[0]

    base_url = 'http://export.arxiv.org/api/query?'
    params = 'search_query=cat:' + cat
    arxiv_api_url = base_url + params

    response = requests.get(arxiv_api_url).content
    feed = feedparser.parse(response)

    feed_info = parse_feed(feed)

    return feed_info

def parse_feed(feed):
    coords = []
    authors = []
    countries = defaultdict(int)

    for entry in feed.entries:

        # if hasattr(entry, "authors"):
        #     for author in entry.authors:
        #         print(author)
        #         info = get_author_info(author.name)


        if hasattr(entry, "arxiv_affiliation") and hasattr(entry, "authors"):
            affiliation = entry.arxiv_affiliation
            print("aff:", affiliation)

            country, coord = get_place_info(affiliation)
            coords.append(coord)

            countries[country] += 1
        
            print("authors:", entry.authors)
            # authors.append([author.name for author in entry.authors])

    print(coords)

    json_response = {
        'countries': list(countries.items()),
        'coords': coords_to_geojson(coords)
    }

    return jsonify(json_response)


# def get_author_info(author):
#     base_url = "https://scholar.google.com/citations"
#     params = "?view_op=search_authors&hl=en&mauthors=" + urllib.parse.quote(author) + "&btnG="
#     scholar_url = base_url + params

#     response = requests.get(scholar_url).content
    
#     doc = lxml.html.document_fromstring(response)
#     # print(doc.text_content())
#     el = doc.xpath("//div[@class='gsc_1usr']")

#     print(el)
#     if el:
#         print(el[0])
#         info = el[0].xpath("div[@class='gsc_oai_aff']")

#         if info:
#             info_text = info[0].text_content()
#             print(info_text)

#     return None


def get_author_info(author):
    base_url = "https://www.researchgate.net/search.Search.html"
    params = "?type=researcher&query=" + author
    rg_url = base_url + params


    driver = webdriver.PhantomJS()
    driver.get(rg_url)
    print(driver.text)
    # p_element = driver.find_element_by_id(id_='intro-text')
    # print(p_element.text)

    

    # response = requests.get(rg_url).content
    
    # doc = lxml.html.document_fromstring(response)
    # print(doc.text_content())
    el = driver.find_elements_by_class_name("nova-v-person-item__stack-item")

    print("el", el)
    # if el:
    #     print("el[0]", el[0])
    #     info = el[0].xpath("div[@class='nova-e-text']")

        

    #     if info:
    #         info_text = info[0].text_content()
    #         print("info_text", info_text)

    #         li = info[0].xpath("li[@class='nova-e-list__item']")
    #         print(li)

    return None

def coords_to_geojson(coords):
    features = []

    for longitude, latitude in coords:
        feature = Feature(geometry = Point((longitude, latitude)))
        features.append(feature)

    collection = FeatureCollection(features)

    return collection

def get_place_info(place):
    access_token = "pk.eyJ1Ijoia2FzbmV6ZGUiLCJhIjoiY2lzZzN0bXMyMDA0bDJwbnF1d3o0cno1OSJ9.wYotm4p930IHqUht229Acw"
    base_url = "https://api.mapbox.com/geocoding/v5/mapbox.places/"

    mapbox_api_url = base_url + urllib.parse.quote(place) + ".json?access_token=" + access_token
    print("calling", mapbox_api_url)
    response = requests.get(mapbox_api_url).content

    data = json.loads(response)

    # TODO select best
    best_response = data['features'][0]

    center = best_response['center']
    country = best_response['context'][-1]['text']

    return country, tuple(center)


