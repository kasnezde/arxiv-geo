from flask_restplus import Resource, Api

api = Api(version='1.0', title='arXiv Geo API',
          description='A simple API for arXiv Geo')

@api.route('/api/categories')
class Categories(Resource): 
    def get(self):
        return jsonify({'categories': categories})