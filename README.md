# arXiv Geo

This application *maps* the state of the art in an unusual way: *it actually shows the research on the map!*

**arXiv Geo** uses API of the e-Print archive [arXiv.org](https://www.arxiv.org) to extract information about the origin of the scientific papers and displays the information about the location of the origin on an interactive map.